# Posni MASZ Invitation Tracker

A very nice and fine invitation tracker for MASZ.

## build

`mvn install`

## setup

Please set following evn-variables for DB and Discord connection:
- MASZ_INV_TRACKER_DB_USER
- MASZ_INV_TRACKER_DB_PW
- MASZ_INV_TRACKER_TOKEN

## run

`mvn exec:java`
