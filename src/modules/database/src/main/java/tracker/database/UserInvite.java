package tracker.database;

import lombok.AllArgsConstructor;
import lombok.Value;

import java.sql.Date;

@Value
@AllArgsConstructor
public class UserInvite
{
    String guildId;
    String targetChannelId;
    String joinedUserID;
    String usedInvite;
    String inviteIssuerId;
    Date joinedAt;
    Date inviteCreatedAt;
}
