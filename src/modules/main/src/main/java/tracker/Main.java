package tracker;

import tracker.cli.Shell;
import lombok.extern.log4j.Log4j;
import tracker.database.SqlConnector;
import tracker.discord.InvitationTracker;

import javax.security.auth.login.LoginException;


@Log4j
public class Main
{
    public static void main(String[] args)
    {
        log.info("Init System");
        SqlConnector.setLoginName(System.getenv("MASZ_INV_TRACKER_DB_USER"));
        SqlConnector.setLoginPw(System.getenv("MASZ_INV_TRACKER_DB_PW"));
        if (!SqlConnector.testConnection())
        {
            log.warn("Could not connect to DB, did you set MASZ_INV_TRACKER_DB_USER and MASZ_INV_TRACKER_DB_PW?");
            return;
        }

        try
        {
            var invitationTracker = new InvitationTracker(System.getenv("MASZ_INV_TRACKER_TOKEN"));
            var shell = new Shell(invitationTracker, invitationTracker);
            shell.start();
        } catch (LoginException e)
        {
            log.warn("Could not connect to Discord, did you set MASZ_INV_TRACKER_TOKEN?", e);
            return;
        } catch (InterruptedException e)
        {
            Thread.currentThread().interrupt();
            log.error("JDA interrupted", e);
            return;
        }

        log.info("System is running fine");
    }
}
